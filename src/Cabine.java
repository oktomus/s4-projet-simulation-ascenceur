public class Cabine extends Constantes {

	public Etage etage; // actuel

	public boolean porteOuverte;

	private char status; // '-' ou 'v' ou '^'
	
	private int nbPassager;

	private Passager[] tableauPassager;

	public Cabine(Etage e) {
		assert e != null;
		etage = e;
		nbPassager = 0;
		tableauPassager = new Passager[nombreDePlacesDansLaCabine];
		porteOuverte = false;
		status = 'v';
	}

	public void afficheLaSituation() {
		System.out.print("Contenu de la cabine: ");
		for (int i = tableauPassager.length - 1; i >= 0 ; i--) {
			Passager p = tableauPassager[i];
			if (p != null) {
				p.affiche();
				System.out.print(' ');
			}
		}
		assert (status == 'v') || (status == '^') || (status == '-');
		System.out.println("\nStatus de la cabine: " + status);
	}

	public char status() {
		assert (status == 'v') || (status == '^') || (status == '-');
		return status;
	}

	public boolean ajouterPassager(Passager p){
		assert p.etageDepart().numero() == etage.numero();
		if(this.estPleine()) return false;
		// Si on est en mode parfait et que le passager va dans le même sens que la cabine
		if (isModeParfait() && ((p.etageDestination().numero() > etage.numero() && status == 'v') || (p.etageDestination().numero() < etage.numero() && status == '^'))) {
			return false;
		}
		for (int i = 0; i < tableauPassager.length; i++) {
			if(tableauPassager[i] == null){
				tableauPassager[i] = p;
				nbPassager++;
				return true;
			}
		}

		return false;
	}

	public boolean passagerVeutSortir(){
		for (int i = 0; i < tableauPassager.length; i++) {
			if(tableauPassager[i] != null &&  tableauPassager[i].etageDestination().numero() == etage.numero())
				return true;
		}
		return false;
	}

	public int sortirPassager(Evenement e, Immeuble im){
		int nbPassagersSortis = 0;
		for (int i = 0; i < tableauPassager.length; i++) {
			if(tableauPassager[i] != null && tableauPassager[i].etageDestination().numero() == etage.numero()){
				im.cumulDesTempsDeTransport += e.date - tableauPassager[i].dateDepart();   
				tableauPassager[i] = null;
				nbPassagersSortis++;
				this.nbPassager--;
			}
		}
		return nbPassagersSortis;
	}

	public void changerStatus(char s){
		assert (s == 'v') || (s == '^') || (s == '-');
		status = s;
	}

	public void bouger( Echeancier echeancier, long date){
		// MONTER
		if(this.status()=='^' && etage.plus_haut != null || this.status()=='v' && etage.plus_bas == null){
			this.changerStatus('^');
			EvenementPassageCabinePalier e = new EvenementPassageCabinePalier(date+tempsPourBougerLaCabineDUnEtage, etage.plus_haut);
			echeancier.ajouter(e);
			// DESCENDRE
		}else if(this.status()=='v' && etage.plus_bas != null || this.status()=='^' && etage.plus_haut == null){
			this.changerStatus('v');
			EvenementPassageCabinePalier e = new EvenementPassageCabinePalier(date+tempsPourBougerLaCabineDUnEtage, etage.plus_bas);
			echeancier.ajouter(e);
		}   
	}

	public void definirNouveauSens(Immeuble im) {
		boolean monterCabine = false, 
				descendreCabine = false,
				attenteHaut = false,
				attenteBas = false,
				passagerAttenteMonter = false,
				passagerAttenteDescendre = false,
				monter = false,
				descendre = false;
		
			
			for (int i = 0; i < tableauPassager.length; i++) {
				if(tableauPassager[i] != null && tableauPassager[i].etageDestination().numero() > etage.numero()){
					monterCabine = true;
				}
				else if(tableauPassager[i] != null && tableauPassager[i].etageDestination().numero() < etage.numero()) {
					descendreCabine = true;
				}
				if(monterCabine && descendreCabine){
					break;
				}
			}

		// Si personne dans la cabine veut monter ou descendre
		if(!isModeParfait() || !monterCabine && !descendreCabine){
			// TRUE SI des personnes attendent au dessus
			attenteHaut = im.passagerEnAttenteDessus();
			// TRUE si des personnnes attendent au dessous
			attenteBas = im.passagerEnAttenteDessous();
			// Si personne n'attent en haut ou en bas
			if(!attenteHaut && !attenteBas){
				passagerAttenteMonter = etage.passagerAttentePourDessus();
				passagerAttenteDescendre = etage.passagerAttentePourDessous();	
			}
		}
		/*		
		System.out.println("--------------");
		System.out.println("monterCabine is " + monterCabine);
		System.out.println("attenteHaut is " + attenteHaut);
		System.out.println("passagerAttenteMonter is " + passagerAttenteMonter);
		System.out.println("descendreCabine is " + descendreCabine);
		System.out.println("attenteBas is " + attenteBas);
		System.out.println("passagerAttenteDescendre is " + passagerAttenteDescendre);
		System.out.println("--------------");*/
		monter = monterCabine || attenteHaut || passagerAttenteMonter;
		descendre = descendreCabine || attenteBas || passagerAttenteDescendre;

		if (monter && !descendre) {
			this.changerStatus('^');
		}else if(!monter && descendre) {
			this.changerStatus('v');
		}
	}

	public boolean estPleine() {
		return this.nbPassager == tableauPassager.length;
	}

}
