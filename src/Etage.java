import java.util.ArrayList;
import java.util.Iterator;

public class Etage extends Constantes {

	private int numero; // de l'Etage pour l'usager

	public Etage plus_haut; // ou null sinon

	public Etage plus_bas; // ou null sinon

	private LoiDePoisson poissonFrequenceArrivee; // dans l'Etage

	private ArrayList<Passager> listePassagersEtage = new ArrayList<Passager>();

	public Etage(Etage pb, int n, int fa) {
		plus_bas = pb;
		numero = n;
		int germe = n << 2;
		if (germe <= 0) {
			germe = -germe + 1;
		}
		poissonFrequenceArrivee = new LoiDePoisson(germe, fa);
	}
	
	public boolean passagerAttentePourDessus(){
		for (Passager passager : listePassagersEtage) {
			if(passager.etageDestination().numero > numero){
				return true;
			}
		}
		return false;
	}
	
	public boolean passagerAttentePourDessous(){
		for (Passager passager : listePassagersEtage) {
			if(passager.etageDestination().numero < numero){
				return true;
			}
		}
		return false;
	}

	public void afficheLaSituation(Immeuble immeuble) {
		if (numero() >= 0) {
			System.out.print(' ');
		}
		System.out.print(numero());
		if (this == immeuble.cabine.etage) {
			System.out.print(" C ");
			if (immeuble.cabine.porteOuverte) {
				System.out.print("[  ]: ");
			} else {
				System.out.print(" [] : ");
			}
		} else {
			System.out.print("   ");
			System.out.print(" [] : ");
		}
		int i = 0;
		boolean stop = listePassagersEtage.size() == 0;
		while (!stop) {
			if (i >= listePassagersEtage.size()) {
				stop = true;
			} else if (i > 6) {
				stop = true;
				System.out.print("...(");
				System.out.print(listePassagersEtage.size());
				System.out.print(')');
			} else {
				listePassagersEtage.get(i).affiche();
				i++;
				if (i < listePassagersEtage.size()) {
					System.out.print(", ");
				}
			}
		}
		System.out.print('\n');
	}

	public int numero() {
		return this.numero;
	}

	public void ajouter(Passager passager) {
		assert passager != null;
		listePassagersEtage.add(passager);
	}

	public long arriveeSuivante() {
		return poissonFrequenceArrivee.suivant();
	}
	
	public boolean passagerVeutRentrer(Immeuble im){
		// si pas de passager, on retourne false
		if (listePassagersEtage.size() == 0) {
			return false;
		}
		if(listePassagersEtage.size()>0 && (this.numero == im.etageLePlusBas().numero || this.numero == im.etageLePlusHaut().numero)){
			return true;
		}
		if((!im.passagerEnAttenteDessus() && !im.passagerEnAttenteDessous()) && listePassagersEtage.size() > 0) {
			return true;
		}
		for (Passager passager : listePassagersEtage) {
			/*if ((!im.passagerEnAttenteDessus() && passager.etageDestination().numero() > im.cabine.etage.numero()) || 
				(!im.passagerEnAttenteDessous() && passager.etageDestination().numero() < im.cabine.etage.numero())) {
				return true;
			}*/
			if (((passager.etageDestination().numero() < im.cabine.etage.numero() && im.cabine.status() == 'v') || 
				(passager.etageDestination().numero() > im.cabine.etage.numero() && im.cabine.status() == '^'))) {
				return true;
			}
		}
		return false;
	}
	
	public int monterPassagerCabine(Cabine cab, Immeuble im){
		assert cab.etage.numero() == numero();
		int nbPassagersMontes = 0;
		int i = 0;
		while (i < this.listePassagersEtage.size()) {
			if(cab.ajouterPassager(this.listePassagersEtage.get(i))) {
				listePassagersEtage.remove(i);
				nbPassagersMontes++;
			}
			else {
				i++;
			}
		}
		/*for (int i = this.listePassagersEtage.size()-1; i >= 0; i--) {
			if(cab.ajouterPassager(this.listePassagersEtage.get(i))) {
				listePassagersEtage.remove(i);
				nbPassagersMontes++;
			}
		}*/
		if(listePassagersEtage.size()==0){
		 im.enleverPassagerEnAttente(this);
		}
		return nbPassagersMontes;
	}
	
	public Etage destinationPremierPassager(){
		return this.listePassagersEtage.get(0).etageDestination();
	}
	

}
