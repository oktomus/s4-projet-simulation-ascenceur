public class EvenementArriveePassagerPalier extends Evenement {

    private Etage etageDeDepart;
    
    public EvenementArriveePassagerPalier(long d, Etage edd) {
        super(d);
        etageDeDepart = edd;
    }
    
    public void afficheDetails(Immeuble immeuble) {
        System.out.print("APP ");
        System.out.print(etageDeDepart.numero());
    }
    
    public void traiter(Immeuble immeuble, Echeancier echeancier) {
        assert etageDeDepart != null;
    	Passager p = new Passager (date, etageDeDepart, immeuble);
    	assert p.etageDestination() != etageDeDepart;
    	// Si les portes sont ouvertes
    	boolean monteCabine = false;
    	if (etageDeDepart.numero() == immeuble.cabine.etage.numero() && immeuble.cabine.porteOuverte ) {
    		if((monteCabine = immeuble.cabine.ajouterPassager(p))){
    			// SI il y a de la place dans la cabine
    			echeancier.decalerFermeture(tempsPourEntrerDansLaCabine);    			
    		}else{
    			// C'est qu'il y a plus de places
    			assert immeuble.cabine.estPleine() ||
    			(
    					isModeParfait() 
    					&& ((p.etageDestination().numero() > immeuble.cabine.etage.numero() && immeuble.cabine.status() == 'v') || (p.etageDestination().numero() < immeuble.cabine.etage.numero() && immeuble.cabine.status() == '^'))
				);
    		}
    	}
    	if(!monteCabine){
    		// Sinon (si il y a plus de places)
    		etageDeDepart.ajouter(p);
    		immeuble.passagerArrive(etageDeDepart);    		
    	}

    	// Regeneration
    	this.date += etageDeDepart.arriveeSuivante();
    	echeancier.ajouter(this);
    }
    
    public String getType() {
		return "APP";
	}
}
