public class EvenementOuverturePorteCabine extends Evenement {
    
    public EvenementOuverturePorteCabine(long d) {
        super(d);
    }
    
    public void afficheDetails(Immeuble immeuble) {
        System.out.print("OPC");
    }
    
    public void traiter(Immeuble immeuble, Echeancier echeancier) {
        Cabine cabine = immeuble.cabine;
        assert ! cabine.porteOuverte;
        assert cabine.passagerVeutSortir() || cabine.etage.passagerVeutRentrer(immeuble);
        cabine.porteOuverte = true;
        int sortie = cabine.sortirPassager(this, immeuble);
        immeuble.nombreTotalDesPassagersSortis += sortie;
        long tempsSortir =  sortie * tempsPourSortirDeLaCabine;
        long tempsEntrer =  cabine.etage.monterPassagerCabine(cabine, immeuble) * tempsPourEntrerDansLaCabine;
        assert cabine.porteOuverte;
        //cabine.definirNouveauSens();
        Evenement fermeture = new EvenementFermeturePorteCabine(this.date + tempsPourFermerLesPortes + tempsEntrer + tempsSortir);
        echeancier.ajouter(fermeture);
    }
    
    public String getType() {
		return "OPC";
	}
    
}
