public class EvenementPassageCabinePalier extends Evenement {

    private Etage etage;

    public EvenementPassageCabinePalier(long d, Etage e) {
        super(d);
        assert e != null;
        etage = e;
    }

    public void afficheDetails(Immeuble immeuble) {
        System.out.print("PCP ");
        System.out.print(etage.numero());
    }
    
    public void traiter(Immeuble immeuble, Echeancier echeancier) {
        Cabine cabine = immeuble.cabine;
        assert  ! cabine.porteOuverte;
        assert Math.abs(cabine.etage.numero() - etage.numero()) <= 1 && Math.abs(cabine.etage.numero() - etage.numero()) >= 0 ;
        assert etage != null;
        cabine.etage = etage;
        if(cabine.passagerVeutSortir() || cabine.etage.passagerVeutRentrer(immeuble)){
        	cabine.definirNouveauSens(immeuble);
        	EvenementOuverturePorteCabine e = new EvenementOuverturePorteCabine(this.date + tempsPourOuvrirLesPortes);
        	echeancier.ajouter(e);
        }else{        	
        	cabine.bouger( echeancier, this.date);     	
        }
    }
    
    public String getType() {
		return "PCP";
	}
}
